//
//  CRPInfoVC.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPInfoVC.h"

@interface CRPInfoVC ()

@end

@implementation CRPInfoVC

- (id)init {
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBar];
    [self setCloseButton];
    
    UIView *partnerView = [[CRPCardView alloc] initWithType:YES];
    
    UIImageView *partnersView = [[UIImageView alloc] init];
    UIImage *partnersImage = [UIImage imageNamed:@"crp_partners.png"];
    if(sharedAppDelegate.mainScreenI5 == YES) {
        [partnersView setFrame:CGRectMake(10, 23, partnersImage.size.width, partnersImage.size.height)];
    }else {
        [partnersView setFrame:CGRectMake(10, 23, partnersImage.size.width, partnersImage.size.height-65)];
    }
    
    [partnersView setImage:partnersImage];
    
    [partnerView addSubview:partnersView];
    [self.view addSubview:partnerView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
