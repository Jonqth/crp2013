//
//  CRPMainVC.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPMainVC.h"
#import "CRPInfoVC.h"
#import "IIViewDeckController.h"
#import "CRPSplitVC.h"

@interface CRPMainVC () {
    BOOL pageUsed;
    NSMutableArray *buttonArray;
    
    NSArray *dataArray;
    NSDictionary *dataDic;
    UIActivityIndicatorView *activityIndicator;
    NSString *dataURL;
    NSString *junior_id;
    
    NSInteger timeTable;
    NSMutableDictionary *checkDic;
}

@end

@implementation CRPMainVC

// Elements
@synthesize formationList;
@synthesize formationPageControl;

- (id)initWithTimetable:(NSInteger)time andJuniorID:(NSString*)id_junior{
    
    // PanningMode
    [self enablePanningMode];
    
    // Time
    dataURL = [NSString stringWithFormat:@"http://crp2013.com/api/api.php?a=formations&b=%d",time];
    timeTable = time;
    
    // data
    junior_id = id_junior;
    

    NSString *myPath = [self saveFilePath];
    
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:myPath];
    
	if (fileExists) {
		NSArray *values = [[NSArray alloc] initWithContentsOfFile:myPath];
		checkDic = [values objectAtIndex:1];
	}else{
        NSNumber *undone = [NSNumber numberWithInt:0];
        checkDic = [[NSMutableDictionary alloc]
                    initWithObjects:[NSArray arrayWithObjects:undone,undone,undone,undone, nil]
                    forKeys:[NSArray arrayWithObjects:@"one",@"two",@"three",@"four", nil]];
    }
    
    self = [super init];
    if (self) { [self setNetworkingData]; }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIApplication *myApp = [UIApplication sharedApplication];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:myApp];
    
    // Set
    [self setNavigationBar];
    [self setMenuButton];
    [self setInfoButton];
    pageUsed = NO;
}

// Methods
- (void) PageControlChanged {
    CGRect frame;
    frame.origin.x = self.formationList.frame.size.width * self.formationPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.formationList.frame.size;
    [self.formationList scrollRectToVisible:frame animated:YES];
    pageUsed = YES;
}


// Saving
- (NSString *) saveFilePath {
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[path objectAtIndex:0] stringByAppendingPathComponent:@"savefile.plist"];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	NSArray *values = [[NSArray alloc] initWithObjects:junior_id,checkDic,nil];
	[values writeToFile:[self saveFilePath] atomically:YES];
}


// CheckIN
- (void) CheckIN:(id)button {
    UIButton *buttonClicked = button;
    NSArray* params = [buttonClicked.titleLabel.text componentsSeparatedByString: @"/"];
    NSString* id_formation = [params objectAtIndex: 0];
    NSString* id_junior = [params objectAtIndex: 1];
    
    NSNumber *done = [NSNumber numberWithInt:1];
    
    switch (timeTable) {
        case 1:
            [checkDic setValue:done forKey:@"one"];
            break;
        case 2:
            [checkDic setObject:done forKey:@"two"];
            break;
        case 3:
            [checkDic setObject:done forKey:@"three"];
            break;
        case 4:
            [checkDic setObject:done forKey:@"four"];
            break;
            
        default:
            break;
    }

    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://crp2013.com/api/api.php?a=check&b=%@&c=%@",id_junior,id_formation]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        dataArray = JSON;
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    // operation
    [operation start];
    
    int i;
    for(i=0; i<[buttonArray count]; i++){
        [[buttonArray objectAtIndex:i] setEnabled:NO];
    }
    
}


// Networking
- (void) setNetworkingData {
    
    dataArray = [[NSArray alloc] init];
    NSURL *url = [[NSURL alloc] initWithString:dataURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        dataArray = JSON;
        [self UILoading];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    // operation
    [operation start];
    
    // loader
    [self dataLoader];
}

- (void) dataLoader {
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 242, 20, 20)];
    [activityIndicator setBackgroundColor:[UIColor clearColor]];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator startAnimating];
    
    [self.view addSubview:activityIndicator];
}

// Delegate's methods
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!pageUsed) {
        CGFloat pageWidth = self.formationList.frame.size.width;
        int page = floor((self.formationList.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        [self.formationPageControl setCurrentPage:page];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageUsed = NO;
}


- (void) UILoading {
    
    // remove loader
    [activityIndicator startAnimating];
    
    // Button Array
    buttonArray = [[NSMutableArray alloc] init];
    
    // ScrollView
    self.formationList = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.formationList setBackgroundColor:[UIColor clearColor]];
    [self.formationList setDelegate:self];
    [self.formationList setContentSize:CGSizeMake(self.view.frame.size.width * [dataArray count],self.formationList.frame.size.height)];
    [self.formationList setShowsHorizontalScrollIndicator:NO];
    [self.formationList setClipsToBounds:YES];
    [self.formationList setPagingEnabled:YES];
    [self.formationList setCanCancelContentTouches:NO];
    [self.formationList setAlwaysBounceHorizontal:NO];
    
    // Card
    int i;
    for(i = 0; i<[dataArray count]; i++) {
        
        // Data Settings
        dataDic = [dataArray objectAtIndex:i];
        
        // Frame size for iPhone type
        CGRect CardFrame;
        
        if(sharedAppDelegate.mainScreenI5 == YES) {
            CardFrame = CGRectMake((self.formationList.frame.size.width * i)+15, 15, 290, 457.5f);
        }else {
            CardFrame = CGRectMake((self.formationList.frame.size.width * i)+15, 15, 290, 369.5f);
        }
        
        // Card
        UIView *cardView = [[CRPCardView alloc] initForScrollWithFrame:CardFrame];
        
        // Name & Level
        UILabel *nameLevel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, cardView.frame.size.width, 20)];
        [nameLevel setText:[dataDic objectForKey:@"head"]];
        [nameLevel setTextColor:sharedAppDelegate.CRPbackDeepGreen];
        [nameLevel setFont:sharedAppDelegate.CRPSmallFont];
        [nameLevel setBackgroundColor:[UIColor clearColor]];
        [nameLevel setTextAlignment:NSTextAlignmentCenter];
        [cardView addSubview:nameLevel];
        
        int to_h, to_w, c_h, line_n, desc_h;
        if(sharedAppDelegate.mainScreenI5 == YES) {
            to_h = 132;
            c_h = 132;
            to_w = 228;
            line_n = 7;
            desc_h = 86;
        }else {
            to_h = 92;
            c_h = 92;
            to_w = 168;
            line_n = 3;
            desc_h = 42;
        }
        
        // Logo
        UIView *logoContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 35, cardView.frame.size.width, c_h)];
        [logoContainer setBackgroundColor:[UIColor clearColor]];
        UIImageView *logoImageView = [[UIImageView alloc] init];
        [logoImageView setBackgroundColor:[UIColor clearColor]];
        [logoImageView setClipsToBounds:NO];
        
        
        // Getting the image
        NSURL *imageUrl = [NSURL URLWithString:[dataDic objectForKey:@"logo_formation"]];
        [[SDWebImageManager sharedManager] downloadWithURL:imageUrl options:0
                                                  progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                      NSLog(@"loading");
                                                  } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                      image = [self imageScale:image andScaleToHeight:to_h andWidth:to_w];
                                                      [logoImageView setFrame:CGRectMake((logoContainer.frame.size.width/2)-(image.size.width/2),
                                                                                         (logoContainer.frame.size.height/2)-(image.size.height/2),
                                                                                         image.size.width, image.size.height)];
                                                      [logoImageView setImage:image];
                                                  }];
        [logoContainer addSubview:logoImageView];
        [cardView addSubview:logoContainer];
        
        // Timetable
        UILabel *Timetable = [[UILabel alloc] initWithFrame:CGRectMake(10, 8.5, cardView.frame.size.width-20, 20)];
        [Timetable setText:[dataDic objectForKey:@"horaires"]];
        [Timetable setTextColor:[UIColor whiteColor]];
        [Timetable setFont:[UIFont fontWithName:@"Archive" size:16]];
        [Timetable setBackgroundColor:[UIColor clearColor]];
        [Timetable setTextAlignment:NSTextAlignmentCenter];
        
        UIView *timeTableView = [[UIView alloc] initWithFrame:CGRectMake(0, logoContainer.frame.origin.y+(logoContainer.frame.size.height+15),
                                                                         cardView.frame.size.width, 35)];
        [timeTableView setBackgroundColor:[UIColor colorWithRed:(235/255.f) green:(98/255.f) blue:(71/255.f) alpha:1]];
        [timeTableView addSubview:Timetable];
        
        [cardView addSubview:timeTableView];
        
        // Title
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(0, timeTableView.frame.origin.y+(timeTableView.frame.size.height+15)
                                                                   , cardView.frame.size.width, 30)];
        [Title setNumberOfLines:0];
        [Title setText:[dataDic objectForKey:@"titre"]];
        [Title setTextColor:sharedAppDelegate.CRPbackDeepGreen];
        [Title setFont:sharedAppDelegate.CRPCapsFont];
        [Title setBackgroundColor:[UIColor clearColor]];
        [Title setTextAlignment:NSTextAlignmentCenter];
        
        [cardView addSubview:Title];
        
        // Sep
        UIImage *separator = [UIImage imageNamed:@"crp_sep.png"];
        UIImageView *sepImageView = [[UIImageView alloc] initWithImage:separator];
        [sepImageView setFrame:CGRectMake((cardView.frame.size.width/2)-(separator.size.width/2),
                                          Title.frame.origin.y+(Title.frame.size.height+5)
                                          ,separator.size.width, separator.size.height)];
        [cardView addSubview:sepImageView];
        
        // Description
        UILabel *Descritpion = [[UILabel alloc] initWithFrame:CGRectMake(10, sepImageView.frame.origin.y+(sepImageView.frame.size.height+5)
                                                                         ,cardView.frame.size.width-20, desc_h)];
        [Descritpion setNumberOfLines:line_n];
        [Descritpion setText:[dataDic objectForKey:@"description"]];
        [Descritpion setTextColor:sharedAppDelegate.CRPbackDeepGreen];
        [Descritpion setFont:sharedAppDelegate.CRPSmallFont];
        [Descritpion setBackgroundColor:[UIColor clearColor]];
        [Descritpion setTextAlignment:NSTextAlignmentCenter];
        
        [cardView addSubview:Descritpion];
        
        // Sep
        UIImageView *sepImageViewND = [[UIImageView alloc] initWithImage:separator];
        [sepImageViewND setFrame:CGRectMake((cardView.frame.size.width/2)-(separator.size.width/2),
                                            Descritpion.frame.origin.y+(Descritpion.frame.size.height+5)
                                            ,separator.size.width, separator.size.height)];
        [cardView addSubview:sepImageViewND];
        
        // Time test
        NSString *timeTableConditionUP;
        NSString *timeTableConditionDW;
        NSString *cKey;
        switch (timeTable) {
            case 1:
                timeTableConditionUP = @"10:45";
                timeTableConditionDW = @"09:25";
                cKey = @"one";
            break;
            case 2:
                timeTableConditionUP = @"12:30";
                timeTableConditionDW = @"11:10";
                cKey = @"two";
            break;
            case 3:
                timeTableConditionUP = @"16:20";
                timeTableConditionDW = @"15:00";
                cKey = @"three";
            break;
            case 4:
                timeTableConditionUP = @"18:05";
                timeTableConditionDW = @"16:45";
                cKey = @"four";
            break;
                
            default:
            break;
        }
        
        // Time test
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
        [formatter setLocale:local];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Paris"]];
        
        NSDate *nowDate = [NSDate date];
        NSCalendar *calendarNOW = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponentsNOW = [calendarNOW components:NSHourCalendarUnit fromDate:nowDate];
       
        NSDate *formUPDate = [formatter dateFromString:[NSString stringWithFormat:@"23/03/13 %@",timeTableConditionUP]];
        NSCalendar *calendarUP = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponentsUP = [calendarUP components:NSHourCalendarUnit fromDate:formUPDate];
        
        NSDate *formDWDate = [formatter dateFromString:[NSString stringWithFormat:@"23/03/13 %@",timeTableConditionDW]];
        NSCalendar *calendarDW = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponentsDW = [calendarDW components:NSHourCalendarUnit fromDate:formDWDate];

        BOOL displayButton = '\0';
        if([dateComponentsNOW hour] <= [dateComponentsUP hour] && [dateComponentsNOW hour] > [dateComponentsDW hour]){
            if([[checkDic objectForKey:cKey] intValue] == 1){ displayButton = NO; }
            else{ displayButton = YES; }
        }else {
            displayButton = NO;
        }
        
        // Check-in
        UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonArray addObject:checkButton];
        UIImage *checkButtonImage = [UIImage imageNamed:@"crp_chekbt.png"];
        UIImage *checkButtonImageH = [UIImage imageNamed:@"crp_chekbt_h.png"];
        
        [checkButton setFrame:CGRectMake(((cardView.frame.size.width/2)-(checkButtonImage.size.width/2)),
                                         sepImageViewND.frame.origin.y+(sepImageViewND.frame.size.height+10),
                                         checkButtonImage.size.width, checkButtonImage.size.height)];
    
        [checkButton setTitle:[NSString stringWithFormat:@"%@/%@",[dataDic objectForKey:@"id_formation"],junior_id] forState:UIControlStateNormal];
        [checkButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [checkButton setBackgroundImage:checkButtonImage forState:UIControlStateNormal];
        [checkButton setBackgroundImage:checkButtonImageH forState:UIControlStateHighlighted];
        [checkButton setEnabled:displayButton];
        [checkButton addTarget:self action:@selector(CheckIN:) forControlEvents:UIControlEventTouchUpInside];
        
        [cardView addSubview:checkButton];
        
        [self.formationList addSubview:cardView];
    }
    
    [self.view addSubview:self.formationList];
    
    // PageControl
    self.formationPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0,self.formationList.frame.size.height+5,self.view.frame.size.width,45)];
    [self.formationPageControl setBackgroundColor:[UIColor clearColor]];
    [self.formationPageControl setNumberOfPages:[dataArray count]];
    [self.formationPageControl setCurrentPage:0];
    [self.formationPageControl setPageIndicatorTintColor:[UIColor colorWithRed:(8/255.f) green:(54/255.f) blue:(51/255.f) alpha:1]];
    [self.formationPageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
    [self.formationPageControl addTarget:self action:@selector(PageControlChanged) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.formationPageControl];
}

 /*
 // Global Ui actions
 // : Selector
 */


- (void) showInfoView {
    CRPInfoVC *infoView = [[CRPInfoVC alloc] init];
    [self presentCurrentView:infoView];
}



////////////////////////////////////////////////

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
