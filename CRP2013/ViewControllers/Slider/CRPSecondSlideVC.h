//
//  CRPSecondSlideVC.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPViewController.h"

@interface CRPSecondSlideVC : CRPViewController <UITableViewDelegate, UITableViewDataSource>

// Elements
@property (strong, nonatomic) UITableView *juniorTableView;

@end
