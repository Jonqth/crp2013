//
//  CRPSplitVC.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 07/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRPSplitVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

// Elements
@property (strong, nonatomic) UITableView *timeTableView;

@end
