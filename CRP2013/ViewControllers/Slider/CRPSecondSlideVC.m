//
//  CRPSecondSlideVC.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPSecondSlideVC.h"
#import "CRPMainVC.h"

@interface CRPSecondSlideVC () {
    NSArray *dataArray;
    NSDictionary *dataDic;
    UIActivityIndicatorView *activityIndicator;
    NSString *dataURL;
}
@end

@implementation CRPSecondSlideVC

// Elements
@synthesize juniorTableView;

- (id)init {
    
    dataURL = @"http://crp2013.com/api/api.php?a=juniors";
    
    self = [super init];
    if (self) {
        [self setNetworkingData];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) UILoading {
    UIView *SecondSlide = [[CRPCardView alloc] initWithType:NO];
    
    // Header
    UIImage *contentSlideImage = [UIImage imageNamed:@"crp_secondslide.png"];
    UIImageView *contentSlideImageView = [[UIImageView alloc] initWithImage:contentSlideImage];
    [contentSlideImageView setFrame:CGRectMake(((SecondSlide.frame.size.width/2)-(contentSlideImage.size.width/2)), 15,
                                               contentSlideImage.size.width, contentSlideImage.size.height)];
    
    [SecondSlide addSubview:contentSlideImageView];
    
    // TableView
    int table_h;
    if(sharedAppDelegate.mainScreenI5 == YES) {
        table_h = 420;
    } else {
        table_h = 332;
    }
    
    self.juniorTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (contentSlideImageView.frame.origin.y+contentSlideImage.size.height+15),
                                                                         SecondSlide.frame.size.width, table_h) style:UITableViewStylePlain];
    [self.juniorTableView setBackgroundColor:[UIColor clearColor]];
    [self.juniorTableView setDelegate:self];
    [self.juniorTableView setDataSource:self];
    [self.juniorTableView setSeparatorColor:sharedAppDelegate.CRPbackDeepGreen];
    
    [SecondSlide addSubview:self.juniorTableView];
    
    
    // Final adding
    [self.view addSubview:SecondSlide];
}

- (void) setNetworkingData {
    
    dataArray = [[NSArray alloc] init];
    NSURL *url = [[NSURL alloc] initWithString:dataURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        dataArray = JSON;
        [self UILoading];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    // operation
    [operation start];
    
    // loader
    [self dataLoader];
}

- (void) dataLoader {
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 242, 20, 20)];
    [activityIndicator setBackgroundColor:[UIColor clearColor]];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator startAnimating];
    
    [self.view addSubview:activityIndicator];
}


/*
 ////////////////////////////////////////////////////////////////////////
 // TableView Methods
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
    [theCell.contentView setBackgroundColor:[UIColor whiteColor]];
    [theCell setBackgroundColor:[UIColor whiteColor]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dataID = [dataArray objectAtIndex:indexPath.row];
    [self toMainView:[dataID objectForKey:@"id_junior"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [dataArray count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88.f;
}


 //Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    // Getting the image
    dataDic = [dataArray objectAtIndex:indexPath.row];
    NSURL *imageUrl = [NSURL URLWithString:[dataDic objectForKey:@"logo_junior"]];
    [[SDWebImageManager sharedManager] downloadWithURL:imageUrl options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                 
                                              } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                  image = [self imageScale:image andScaleToHeight:84 andWidth:150];
                                                  UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(103, 2, 84, 84)];
                                                  [logoView setBackgroundColor:[UIColor whiteColor]];
                                                  [logoView setImage:image];
                                                  [logoView setClipsToBounds:NO];
                                                  [cell.contentView addSubview:logoView];
                                              }];
    
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    myBackView.backgroundColor = sharedAppDelegate.CRPbackDeepGreen;
    cell.selectedBackgroundView = myBackView;
    
    return cell;
}

- (void) toMainView:(NSString *)id_junior {
    CRPMainVC *toMainView = [[CRPMainVC alloc] initWithTimetable:1 andJuniorID:id_junior];
    [self pushToNextView:toMainView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
