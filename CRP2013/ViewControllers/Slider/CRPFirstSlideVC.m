//
//  CRPFirstSlideVC.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPFirstSlideVC.h"
#import "CRPSecondSlideVC.h"

@interface CRPFirstSlideVC ()

@end

@implementation CRPFirstSlideVC

- (id)init {
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *FirstSlide = [[CRPCardView alloc] initWithType:NO];
    
    UIImage *contentSlideImage = [UIImage imageNamed:@"crp_firstslide.png"];
    UIImageView *contentSlideImageView = [[UIImageView alloc] initWithImage:contentSlideImage];
    [contentSlideImageView setFrame:CGRectMake(((FirstSlide.frame.size.width/2)-(contentSlideImage.size.width/2)), 30,
                                               contentSlideImage.size.width, contentSlideImage.size.height)];
    
    UIButton *chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *chooseButtonImage = [UIImage imageNamed:@"crp_choosebt.png"];
    UIImage *chooseButtonImageH = [UIImage imageNamed:@"crp_choosebt_h.png"];

    [chooseButton setFrame:CGRectMake(((FirstSlide.frame.size.width/2)-(chooseButtonImage.size.width/2)),
                                      FirstSlide.frame.size.height-(chooseButtonImage.size.height+35),
                                      chooseButtonImage.size.width, chooseButtonImage.size.height)];
    
    [chooseButton setBackgroundImage:chooseButtonImage forState:UIControlStateNormal];
    [chooseButton setBackgroundImage:chooseButtonImageH forState:UIControlStateHighlighted];
    [chooseButton addTarget:self action:@selector(toNextSlide) forControlEvents:UIControlEventTouchUpInside];
    
    [FirstSlide addSubview:contentSlideImageView];
    [FirstSlide addSubview:chooseButton];
    [self.view addSubview:FirstSlide];
    
    
}

- (void) toNextSlide {
    CRPSecondSlideVC *toSecondSlide = [[CRPSecondSlideVC alloc] init];
    [self pushToNextView:toSecondSlide];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
