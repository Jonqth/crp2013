//
//  CRPSplitVC.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 07/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPSplitVC.h"
#import "CRPMainVC.h"
#import "IIViewDeckController.h"

@interface CRPSplitVC () {
    NSMutableArray *dataArray;
}
@end

@implementation CRPSplitVC

// Elements
@synthesize timeTableView;

- (id)init {
    
    dataArray = [[NSMutableArray alloc] initWithObjects:@"09h25 - 10h45",@"11h10 - 12h30", @"15h00 - 16h20", @"16h45 - 18h05",nil];
    
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Color
    [self.view setBackgroundColor:sharedAppDelegate.CRPbackDeepGreen];
    
    [self UILoading];
}

- (void) UILoading {
    
    
    // TopView
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [topView setBackgroundColor:[UIColor colorWithRed:(169/255.f) green:(66/255.f) blue:(46/255.f) alpha:1]];
    [self.view addSubview:topView];
    
    UIImage *hTImage = [UIImage imageNamed:@"crp_horairestitle.png"];
    UIImageView *horairesTitle = [[UIImageView alloc] initWithImage:hTImage];
    [horairesTitle setFrame:CGRectMake(22, 16, hTImage.size.width, hTImage.size.height)];
    [topView addSubview:horairesTitle];
    
    // TableView
    self.timeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (topView.frame.origin.y+topView.frame.size.height),
                                                                         self.view.frame.size.width, 186) style:UITableViewStylePlain];
    [self.timeTableView setBackgroundColor:[UIColor clearColor]];
    [self.timeTableView setDelegate:self];
    [self.timeTableView setDataSource:self];
    [self.timeTableView setSeparatorColor:[UIColor colorWithRed:(16/255.f) green:(126/255.f) blue:(117/255.f) alpha:1.f]];
    
    [self.view addSubview:self.timeTableView];
    
    // Flag
    UIImage *flagImage = [UIImage imageNamed:@"crp_ecusson.png"];
    UIButton *flagImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [flagImageButton setFrame:CGRectMake(17, (self.timeTableView.frame.origin.y+self.timeTableView.frame.size.height+15),
                                       flagImage.size.width, flagImage.size.height)];
    [flagImageButton setBackgroundImage:flagImage forState:UIControlStateNormal];
    [flagImageButton addTarget:self action:@selector(toWebsite) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:flagImageButton];
    
}

- (void) toWebsite {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.crp2013.com"]];
}

 /*
 ////////////////////////////////////////////////////////////////////////
 // TableView Methods
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* theCell = [tableView cellForRowAtIndexPath:indexPath];
    theCell.contentView.backgroundColor = [UIColor colorWithRed:(5/255.f) green:(32/255.f) blue:(30/255.f) alpha:1.f];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [sharedAppDelegate.appDeckViewController closeLeftViewBouncing:^(IIViewDeckController *controller){
        CRPMainVC *toController = [[CRPMainVC alloc] initWithTimetable:indexPath.row+1 andJuniorID:@"1"];
        [sharedAppDelegate.CRPNavigationController pushViewController:toController animated:YES];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [dataArray count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}


//Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Timetable
    UILabel *Timetable = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, cell.frame.size.width-20, 20)];
    [Timetable setText:[dataArray objectAtIndex:indexPath.row]];
    [Timetable setTextColor:[UIColor colorWithRed:(16/255.f) green:(126/255.f) blue:(117/255.f) alpha:1.f]];
    [Timetable setFont:[UIFont fontWithName:@"Archive" size:14]];
    [Timetable setBackgroundColor:[UIColor clearColor]];
    
    UIImage *accessoryImage = [UIImage imageNamed:@"crp_accessory.png"];
    UIImageView *accessoryView = [[UIImageView alloc] initWithImage:accessoryImage];
    [accessoryView setFrame:CGRectMake(115, 5, accessoryImage.size.width, accessoryImage.size.height)];
    [Timetable addSubview:accessoryView];
    
    [cell addSubview:Timetable];

    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
