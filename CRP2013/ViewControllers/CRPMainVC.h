//
//  CRPMainVC.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRPViewController.h"

@interface CRPMainVC : CRPViewController <UIScrollViewDelegate>

// Methods
- (id)initWithTimetable:(NSInteger)time andJuniorID:(NSString *)id_junior;

// UI elements
@property (strong, nonatomic) UIScrollView *formationList;
@property (strong, nonatomic) UIPageControl *formationPageControl;

@end
