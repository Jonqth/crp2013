//
//  CRPAppDelegate.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRPFirstSlideVC;
@class IIViewDeckController;
@class CRPSplitVC;
@class CRPMainVC;

@interface CRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CRPFirstSlideVC *CRPMainSlideController;

// Navigation
@property (strong, nonatomic) UINavigationController *CRPNavigationController;
@property (strong, nonatomic) IIViewDeckController *appDeckViewController;
@property (strong, nonatomic) CRPSplitVC *appSplitView;
@property (strong, nonatomic) CRPMainVC *appMainView;

// Vars
@property (assign, nonatomic) BOOL mainScreenI5;
@property (strong, nonatomic) UIColor *CRPbackGreen;
@property (strong, nonatomic) UIColor *CRPbackOrange;
@property (strong, nonatomic) UIColor *CRPbackDeepGreen;
@property (strong, nonatomic) UIFont *CRPCapsFont;
@property (strong, nonatomic) UIFont *CRPSmallFont;
@property (strong, nonatomic) NSString *mainJuniorID;

@end
