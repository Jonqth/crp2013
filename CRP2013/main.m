//
//  main.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CRPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CRPAppDelegate class]));
    }
}
