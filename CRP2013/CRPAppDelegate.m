//
//  CRPAppDelegate.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPAppDelegate.h"
#import "CRPFirstSlideVC.h"
#import "IIViewDeckController.h"
#import "CRPSplitVC.h"
#import "CRPMainVC.h"

@implementation CRPAppDelegate

// Navigation
@synthesize CRPNavigationController;

// Vars
@synthesize mainScreenI5;
@synthesize CRPbackGreen;
@synthesize CRPbackOrange;
@synthesize CRPbackDeepGreen;
@synthesize CRPSmallFont;
@synthesize CRPCapsFont;
@synthesize mainJuniorID;

- (id)init {
    
    // Screen iPhone 4/5
    if([[UIScreen mainScreen]bounds].size.height == 568){ self.mainScreenI5 = YES; }
    else{ self.mainScreenI5 = NO; }
    
    // Colors
    self.CRPbackGreen = [UIColor colorWithRed:(10/255.f) green:(77/255.f) blue:(72/255.f) alpha:1];
    self.CRPbackOrange = [UIColor colorWithRed:(235/255.f) green:(98/255.f) blue:(71/255.f) alpha:1];
    self.CRPbackDeepGreen = [UIColor colorWithRed:(7/255.f) green:(46/255.f) blue:(43/255.f) alpha:1];
    
    // Font
    self.CRPCapsFont = [UIFont fontWithName:@"Trend Sans" size:14];
    self.CRPSmallFont = [UIFont fontWithName:@"Trend Sans" size:10];
    
    NSString *myPath = [self saveFilePath];
    
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:myPath];
    
	if (fileExists) { 
		NSArray *values = [[NSArray alloc] initWithContentsOfFile:myPath];
		self.mainJuniorID = [values objectAtIndex:0];
	}

    
    return self;    
}

- (NSString *) saveFilePath {
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[path objectAtIndex:0] stringByAppendingPathComponent:@"savefile.plist"];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = self.CRPbackGreen;
    [self.window makeKeyAndVisible];
    
    // Navigation Controller
    if(self.mainJuniorID != nil){
        self.appMainView = [[CRPMainVC alloc] initWithTimetable:1 andJuniorID:self.mainJuniorID];
        self.CRPNavigationController = [[UINavigationController alloc] initWithRootViewController:self.appMainView];
    }else{
        self.CRPMainSlideController = [[CRPFirstSlideVC alloc] init];
        self.CRPNavigationController = [[UINavigationController alloc] initWithRootViewController:self.CRPMainSlideController];
    }
    
    [self.CRPNavigationController setNavigationBarHidden:YES];
    
    // Split View
    self.appSplitView = [[CRPSplitVC alloc] init];
    
    // DeckView init
    self.appDeckViewController = [[IIViewDeckController alloc] initWithCenterViewController:self.CRPNavigationController
                                                                          leftViewController:self.appSplitView
                                                                         rightViewController:nil];
    [self.appDeckViewController setPanningMode:IIViewDeckNoPanning];
    [self.appDeckViewController setLeftSize: 176.f];
    self.appDeckViewController.bounceOpenSideDurationFactor = 1.2f;
    
    self.window.rootViewController = self.appDeckViewController;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
