//
//  CRPCardView.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPCardView.h"

@implementation CRPCardView

- (id)initWithType:(BOOL)Type {
    self = [super init];
    
    //reset
    self.layer.shadowColor = nil;
    
    
    // Frame size for iPhone type
    CGRect CardFrame;
    
    if(Type == NO){
        if(sharedAppDelegate.mainScreenI5 == YES) {
            CardFrame = CGRectMake(15, 15, 290, 516);
        } else {
            CardFrame = CGRectMake(15, 15, 290, 430);
        }
    }else {
        if(sharedAppDelegate.mainScreenI5 == YES) {
            CardFrame = CGRectMake(15, 59, 290, 457.5f);
        } else {
            CardFrame = CGRectMake(15, 59, 290, 369.5f);
        }
    }
    
    
    [self setFrame:CardFrame];
    
    // Frame details
    [self setBackgroundColor:[UIColor whiteColor]];
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.4;
    self.layer.shadowRadius = 8.0;
    self.layer.shadowOffset = CGSizeMake(.0f, 3.0f);
    
    
    UIView *CRPCardFooter = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-5, self.frame.size.width, 5)];
    [CRPCardFooter setBackgroundColor:sharedAppDelegate.CRPbackOrange];
    [self addSubview:CRPCardFooter];
    
    return self;
}

- (id) initForScrollWithFrame:(CGRect)frame {
    self = [super init];
    
    [self setFrame:frame];
    
    // Frame details
    [self setBackgroundColor:[UIColor whiteColor]];
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.4;
    self.layer.shadowRadius = 6.0;
    self.layer.shadowOffset = CGSizeMake(.0f, 3.0f);
    
    
    UIView *CRPCardFooter = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-5, self.frame.size.width, 5)];
    [CRPCardFooter setBackgroundColor:sharedAppDelegate.CRPbackOrange];
    [self addSubview:CRPCardFooter];

    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) { }
    
    return self;
}


@end
