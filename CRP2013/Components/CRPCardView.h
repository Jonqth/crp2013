//
//  CRPCardView.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CRPCardView : UIView

// Methods
- (id) initWithType:(BOOL)Type;
- (id) initForScrollWithFrame:(CGRect)frame;

@end
