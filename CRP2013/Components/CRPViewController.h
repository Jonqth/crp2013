//
//  CRPViewController.h
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CRPCardView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AFNetworking.h"

@class IIViewDeckController;

@interface CRPViewController : UIViewController

// Controller
@property (strong, nonatomic) IIViewDeckController *appDeckViewController;


// Navigation elements
@property (strong,nonatomic) UINavigationBar *CRPNavBar;
@property (strong,nonatomic) UINavigationItem *CRPNavItem;

// Methods
- (void) enablePanningMode;
- (void) setNavigationBar;
- (void) setMenuButton;
- (void) setInfoButton;
- (void) setCloseButton;
- (void) pushToNextView:(UIViewController *)toController;
- (void) presentCurrentView:(UIViewController *)currentViewController;
- (UIImage*) imageScale:(UIImage *)image andScaleToHeight:(float)s_height andWidth:(float)s_width;
- (void) dismissCurrentView;

@end
