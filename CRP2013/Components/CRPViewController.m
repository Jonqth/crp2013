//
//  CRPViewController.m
//  CRP2013
//
//  Created by Jonathan Araujo-Levy on 06/03/13.
//  Copyright (c) 2013 fuzzze. All rights reserved.
//

#import "CRPViewController.h"
#import "IIViewDeckController.h"

@interface CRPViewController ()

@end

@implementation CRPViewController

// Controller
@synthesize appDeckViewController;

// Elements
@synthesize CRPNavBar;
@synthesize CRPNavItem;


- (id)init {
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:sharedAppDelegate.CRPbackGreen];
}


// **
// Navigation methods
// @superclass InsideViewController
// **

// Push to next view

- (void) pushToNextView:(UIViewController *)toController {
    [self.navigationController pushViewController:toController animated:YES];
}

// Present current ModalView

- (void) presentCurrentView:(UIViewController *)currentViewController {
    [self presentViewController:currentViewController animated:YES completion:nil];
}

// Dismiss current ModalView

- (void) dismissCurrentView {
    [self dismissViewControllerAnimated:YES completion:nil];
}


// **
// Series of elements
// @superclass CRPViewController
// **

- (void) enablePanningMode{
    [sharedAppDelegate.appDeckViewController setPanningMode:IIViewDeckLeftSide];
}

// Set the main NavigationBar

- (void) setNavigationBar {
    self.CRPNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.CRPNavBar setBarStyle:UIBarStyleDefault];
    [self.CRPNavBar setTintColor:sharedAppDelegate.CRPbackOrange];
    [self.CRPNavBar setBackgroundImage:[UIImage imageNamed:@"crp_navbg.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *logoNavView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"crp_titleview.png"]];
    self.CRPNavItem = [[UINavigationItem alloc] init];
    [self.CRPNavItem setTitleView:logoNavView];
    
    [self.CRPNavBar setItems:[NSArray arrayWithObject:self.CRPNavItem] animated:NO];
    [self.view addSubview:self.CRPNavBar];
}

// Set the menu button on a second level view

- (void) setMenuButton {
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *menuButtonButtonImageNormal = [UIImage imageNamed:@"crp_menubt.png"];
    [menuButton setFrame:CGRectMake(0, 0, menuButtonButtonImageNormal.size.width, menuButtonButtonImageNormal.size.height)];
    [menuButton addTarget:self.appDeckViewController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:menuButtonButtonImageNormal forState:UIControlStateNormal];
    [self.CRPNavBar addSubview:menuButton];
}

// Set the menu button on a second level view

- (void) setInfoButton {
    UIButton *InfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *InfoButtonButtonImageNormal = [UIImage imageNamed:@"crp_infobt.png"];
    
    [InfoButton setFrame:CGRectMake(self.view.frame.size.width-InfoButtonButtonImageNormal.size.width, 0,
                                    InfoButtonButtonImageNormal.size.width, InfoButtonButtonImageNormal.size.height)];
    [InfoButton addTarget:self action:@selector(showInfoView) forControlEvents:UIControlEventTouchUpInside];
    [InfoButton setBackgroundImage:InfoButtonButtonImageNormal forState:UIControlStateNormal];
    [self.CRPNavBar addSubview:InfoButton];
}

// Set the close button for info view

- (void) setCloseButton {
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeButtonImageNormal = [UIImage imageNamed:@"crp_closebt.png"];
    
    [closeButton setFrame:CGRectMake(self.view.frame.size.width-closeButtonImageNormal.size.width, 0,
                                     closeButtonImageNormal.size.width, closeButtonImageNormal.size.height)];
    [closeButton addTarget:self action:@selector(dismissCurrentView) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setBackgroundImage:closeButtonImageNormal forState:UIControlStateNormal];
    [self.CRPNavBar addSubview:closeButton];
}

- (UIImage*) imageScale:(UIImage *)image andScaleToHeight:(float)s_height andWidth:(float)s_width {
    
    float oldHeight = 0.0, oldWidth = 0.0,scaleFactor = 0.0,newHeight = 0.0,newWidth = 0.0;
    
    if(image.size.height >= image.size.width){
        // à la française
        oldHeight = image.size.height;
        scaleFactor = s_height / oldHeight;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }else {
        // à l'italienne
        oldWidth = image.size.width;
        scaleFactor = s_width / oldWidth;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
